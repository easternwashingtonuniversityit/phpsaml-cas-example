<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EWU PHP SAML Client Example</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
<h1>PHP SAML Client Example</h1>

<?php

  require_once "./CAS/CAS.php";

  error_reporting(E_ALL & ~E_NOTICE);

  /* CAS Protocol Configuration */
  $cas_host = "sso.ewu.edu";
  $cas_port = 443;
  $cas_context = "/idp/profile/cas";
  
  //phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);
  error_reporting(E_ALL & ~E_NOTICE);
  phpCAS::client(SAML_VERSION_1_1, $cas_host, $cas_port, $cas_context);

  //* Don't validate the CAS Server
  // phpCAS::setDebug();
  // phpCAS::setNoCasServerValidation();

  // DO CAS Authentication
  error_reporting(E_ALL & ~E_NOTICE);
  phpCAS::forceAuthentication();

  // PHP-FPM fix
  if (!function_exists('getallheaders')) {
    function getallheaders() {
      $headers = [];
      foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
          $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
      }
      return $headers;
    }
  }

  // Normal Web Stuff
  $headers = getallheaders();
  ksort($headers);

  print '<h2>Request Headers</h2><ul class="tabs" data-tabs id="header-tabs">';
  foreach($headers as $key => $value){
    print "<li class='tabs-title'><a href='#$key'>$key</a></li>";
  }
  print('</ul><div class="tabs-content" data-tabs-content="header-tabs">');
  foreach($headers as $key => $value){
    print "<div class='tabs-panel' id=\"$key\">";
    print $value;
    print "</div>";

  }
  print('</ul>');

  print('</div><br/>');

  $parameters = $GET;
  print '<h2>Request Parameters</h2><ul class="tabs" data-tabs id="attribute-tabs">';
  foreach($_GET as $key => $value){
    print "<li class='tabs-title'><a href='#$key'>$key</a></li>";
  }
  print('</ul><div class="tabs-content" data-tabs-content="attribute-tabs">');
  foreach($_GET as $key => $value){
    print "<div class='tabs-panel' id=\"$key\">";
    print $value;
    print "</div>";

  }
  print('</ul>');


  print('</div><br/>');

  // Get the User
  error_reporting(E_ALL & ~E_NOTICE);
  $user = phpCAS::getUser();
  print "<h2>One ID</h2>";
  print "<div class='tabs'/>";
  print "<div class='tabs-content'>";
  print "<div class='tabs-panel is-active' aria-hidden='false'>";
  print $user . "</div></div>";

  print('<br/>');

  // Attributes
  print "<h2>Attributes</h2>";
  error_reporting(E_ALL & ~E_NOTICE);
  $attributes = phpCAS::getAttributes();
  ksort($attributes);
  print '<ul class="accordion" data-accordion>';
    foreach ($attributes as $name => $value) {
      print '<li class="accordion-item" data-accordion-item>';
      print '<a class="accordion-title">' . $name . '</a>';
      print '<div class="accordion-content" data-tab-content><pre>';
      echo print_r($value,1);
      print '</pre></div>';
      print '</li>' . PHP_EOL;
    }
  print('</ul>');



?>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>

